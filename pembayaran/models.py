from django.db import models
from django.core.validators import RegexValidator
# Create your models here.
class Order(models.Model):
  order_id=models.IntegerField(primary_key=True, default=0)
  payment=models.CharField(blank=True,null=True,max_length=7)
  delivery=models.BooleanField(default=False)
  date=models.DateField(blank=True,null=True)
  name=models.CharField(blank=True,null=True,max_length=120)
  phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
  phone_number = models.CharField(validators=[phone_regex], max_length=17, blank=True)
  address=models.TextField(blank=True,null=True)
  class Meta:
      verbose_name_plural = "orders"
      
class OrderCounter(models.Model):
  counter=models.IntegerField(default=1)

 
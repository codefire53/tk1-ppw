from django.test import TestCase,Client
from pemesanan.models import Orderedfood
from .forms import OrderForm
from .models import OrderCounter,Order
from sikantin.models import Food
from datetime import date
# Create your tests here.
class OrderTestCase(TestCase):
    def test_index_load(self):
        response = Client().get('/pembayaran/')
        self.assertEqual(response.status_code, 200)
    
    def test_summary(self):
        counter=OrderCounter(counter=2)
        counter.save()
        order=Order(order_id=1)
        order.save()
        food=Food(food_id=1,name='sate',merchant='store',price=0,calories=2.00,counter=1)
        food.save()
        of=Orderedfood(curr_id=1,amount=2,attr=food,order=order)
        of.save()
        response = Client().get('/pembayaran/summary/')
        self.assertEqual(response.status_code, 200)
    
    def test_form_submit(self):
        counter=OrderCounter(counter=2)
        counter.save()
        order=Order(order_id=1)
        order.save()
        c=Client()
        response=c.post('/pembayaran/submit/',{'date':date(2019,10,10),'name':'nezuko','phone_number':'081288915802','address':'pacil','delivery':True,'payment':'Cash'})
        order=Order.objects.get(order_id=1)
        self.assertEqual(response.status_code,302)
        self.assertEqual(order.date,date(2019,10,10))
        self.assertEqual(order.name,'nezuko')
        self.assertEqual(order.phone_number,'081288915802')
        self.assertEqual(order.address,'pacil')
        self.assertEqual(order.payment,'Cash')
        self.assertTrue(order.delivery)
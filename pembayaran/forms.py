from django import forms

class OrderForm(forms.Form):
    date_attrs = {
        'type':'date',
        'class': 'form-control',
    }
    text_attrs={
        'class':'form-control',
        'placeholder':'Tulis "Kantin Fasilkom" jika ingin ambil di tempat'
    }

    PAYMENT=(('GOPay','GOPay'),('Cash','Cash'))
    payment=forms.ChoiceField(choices=PAYMENT,label='Metode Pembayaran')
    delivery=forms.BooleanField(required=False,label='Jasa Pengantaran')
    name=forms.CharField(label="Nama")
    date = forms.DateTimeField(required=True, localize=True, widget=forms.DateInput(attrs=date_attrs),label='Tanggal Pengambilan')
    phone_number = forms.RegexField(regex=r'^\+?1?\d{9,15}$',label="Nomor Telepon")
    address=forms.CharField(widget=forms.Textarea(attrs=text_attrs),label="Alamat Pengambilan")
    
    
    

from django.shortcuts import render,redirect
from .forms import OrderForm
from .models import Order,OrderCounter
from pemesanan.models import Orderedfood
from django.http import HttpResponse
import random
# Create your views here.
def index(request):
    response={'form':OrderForm}
    return render(request,'pembayaran.html',response)

def submit_form(request):
    counter=OrderCounter.objects.get(pk=1)
    counterVal=counter.counter-1
    order=Order.objects.get(order_id=counterVal)
    if(request.method == 'POST'):
        form = OrderForm(request.POST)
        if (form.is_valid()):
            date=form.cleaned_data['date']
            name = form.cleaned_data['name']
            phone_number=form.cleaned_data['phone_number']
            address=form.cleaned_data['address']
            delivery=form.cleaned_data['delivery']
            payment=form.cleaned_data['payment']
            order.payment=payment
            order.delivery=delivery
            order.date=date
            order.name=name
            order.phone_number=phone_number
            order.address=address
            order.save()
    return redirect('/pembayaran/')
    
def summary(request):
    response={}
    delivery_fee= random.randint(1000, 15000) 
    counter=OrderCounter.objects.get(pk=1)
    counterVal=counter.counter-1
    order=Order.objects.get(order_id=counterVal)
    if(order.name==None or order.payment==None or order.date==None or order.address==None or order.phone_number==None):
        return render(request,'pleasefilldata.html')
    response['order']=order
    out=order.orderedfood_set.all()
    total_price=0
    for item in out:
        total_price=total_price+item.attr.price*item.amount
    response['food']=out
    if(order.delivery==False):
        delivery_fee=0
    total_price=total_price+delivery_fee
    response['delivery_fee']=delivery_fee
    response['total_price']=total_price
    return render(request,'summary.html',response)

from django.contrib import admin

# Register your models here.
from .models import Order,OrderCounter

class OrderAdmin(admin.ModelAdmin):
    list_display = ['order_id','payment','delivery','date','name','phone_number','address']

class OrderCounterAdmin(admin.ModelAdmin):
    list_display=['counter']

admin.site.register(Order, OrderAdmin)
admin.site.register(OrderCounter, OrderCounterAdmin)
from django.shortcuts import render,redirect
from django.http import HttpResponse
from .models import Orderedfood
from .forms import AmountForm
from pembayaran.models import OrderCounter,Order
from sikantin.models import Food
# Create your views here.

def index(request):
    response = {"amount_form": AmountForm}
    counter=OrderCounter.objects.get(pk=1)
    counter_val=counter.counter-1
    order=Order.objects.get(order_id=counter_val)
    out=order.orderedfood_set.all()
    response['query']=out
    return render(request,'amount.html',response)

def set_amount(request,curr_id=None,food_id=None):
    response={}
    food=Food.objects.get(food_id=food_id)
    of=Orderedfood.objects.get(curr_id=curr_id,attr=food)
    if(request.method == 'POST'):
        form = AmountForm(request.POST)
        if (form.is_valid()):
            form_content = form.cleaned_data['amount']
            of.amount=form_content
            of.save()
            #form.save()
            #asresponse['amount_form']=form
    #return render(request,'amount.html',response)
    return redirect('/pemesanan/amount')


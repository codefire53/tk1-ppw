from django.test import TestCase,Client
from .models import Orderedfood
from .forms import AmountForm
from pembayaran.models import OrderCounter,Order
from sikantin.models import Food
# Create your tests here.
class PemesananTestCase(TestCase):
    def test_index_request(self):
        counter=OrderCounter(counter=2)
        counter.save()
        order=Order(order_id=1)
        order.save()
        food=Food(food_id=1,name='sate',merchant='store',price=0,calories=2.00,counter=1)
        food.save()
        of=Orderedfood(curr_id=1,amount=2,attr=food,order=order)
        of.save()
        order=Order.objects.get(order_id=1)
        response = Client().get('/pemesanan/amount/')
        self.assertEqual(response.status_code, 200)
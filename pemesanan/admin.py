from django.contrib import admin
# Register your models here.
from .models import Orderedfood

class OrderedFoodAdmin(admin.ModelAdmin):
    list_display = ("curr_id","amount","order")

admin.site.register(Orderedfood, OrderedFoodAdmin)
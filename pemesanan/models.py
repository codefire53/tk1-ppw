from django.db import models
from sikantin.models import Food
from pembayaran.models import Order
# Create your models here.
class Orderedfood(models.Model):
    curr_id=models.PositiveIntegerField(default=1,null=True,blank=True)
    amount=models.PositiveIntegerField(default=1,null=True,blank=True)
    attr=models.OneToOneField(Food,on_delete=models.CASCADE,primary_key=True)
    order=models.ForeignKey(Order,on_delete=models.CASCADE,null=True,blank=True)
    class Meta:
        verbose_name_plural = "ordered_foods"
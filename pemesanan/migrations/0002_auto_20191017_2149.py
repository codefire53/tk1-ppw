# Generated by Django 2.1.5 on 2019-10-17 14:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pemesanan', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='orderedfood',
            options={'verbose_name_plural': 'ordered_foods'},
        ),
    ]

"""tk1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.urls import path
from .views import index,increment,new_order,search_results,homepage
from django.conf.urls.static import static
from django.views.defaults import page_not_found


urlpatterns = [
    path('', index,name="landing"),
    path('search/', search_results, name='search_results'),
    path('home/', homepage, name='home'),
    path('item/counter/<int:item_id>/',increment,name="increment"),
    path('addFood/',new_order,name="new_order"),
    path('404/', page_not_found, {'exception': Exception()})
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

from django.contrib import admin

# Register your models here.
from .models import Food,Search

class FoodAdmin(admin.ModelAdmin):
    list_display = ("food_id","name", "merchant","price","img","calories","counter")

admin.site.register(Food, FoodAdmin)
admin.site.register(Search)
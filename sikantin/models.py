from django.db import models
# Create your models here.
class Food(models.Model):
    food_id=models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    merchant = models.CharField(max_length=50)
    price=models.IntegerField(blank=True,null=True)
    calories=models.DecimalField(max_digits=10,decimal_places=2,null=True)
    counter = models.PositiveIntegerField(default=0, blank=True, null=True)
    img=models.ImageField(upload_to="images",null=True,blank=True)
    class Meta:
      verbose_name_plural = "foods"
    def __str__(self):
        return self.name

class Search(models.Model):
  keyword=models.CharField(max_length=50,null=True,blank=True)
from django.apps import AppConfig


class SikantinConfig(AppConfig):
    name = 'sikantin'

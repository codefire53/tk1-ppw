from django import forms

class SearchForm(forms.Form):
    attrs = {
        'type':'text',
        'class': 'form-control',
        'placeholder': 'Cari disini......'
    }
    keyword = forms.CharField(max_length=25, widget=forms.TextInput(attrs=attrs),label='')

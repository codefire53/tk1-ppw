from django.test import TestCase, Client
from django.urls import resolve
from .models import Food,Search
from pembayaran.models import Order,OrderCounter
from .views import index
from .forms  import SearchForm
from io import BytesIO
from PIL import Image
from django.core.files.base import File
from .views import search_results
# Create your tests here

#url, forms, models, views, template
#assertin
def get_image_file(name='test.png', ext='png', size=(50, 50), color=(256, 0, 0)):
    file_obj = BytesIO()
    image = Image.new("RGBA", size=size, color=color)
    image.save(file_obj, ext)
    file_obj.seek(0)
    return File(file_obj, name=name)
class FoodModelTest(TestCase):
    def test_landing_exist(self):
        client=Client()
        response=client.get('')
        self.assertEqual(response.status_code,200)
        
    def test_form_is_valid(self):
        form_data = {'keyword':'Kata Kunci'}
        form = SearchForm(data = form_data)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['keyword'], 'Kata Kunci')
    
    def test_form_is_not_valid(self):
        form = SearchForm(data = {})
        self.assertFalse(form.is_valid())

    #def test_form_validity(self)
    # def test_view_index(self):
    #     found = resolve('')
    #     self.assertEqual(found.func, index)
    def test_increment_function_works(self):
        obj=Food.objects.create(food_id=1,name="makanan",merchant="toko",price=0,calories=0,counter=0)
        obj.save()
        cnt=OrderCounter.objects.create(counter=1)
        cnt.save()
        obj=Food.objects.get(food_id=1)
        client = Client()
        self.assertEqual(obj.food_id,1)
        response=client.get("/item/counter/1/")
        obj=Food.objects.get(food_id=1)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(obj.counter,1)

    def test_increment_ofnotexist_function_works(self):
        obj=Food.objects.create(food_id=1,name="makanan",merchant="toko",price=0,calories=0,counter=0)
        obj.save()
        cnt=OrderCounter.objects.create(counter=1)
        cnt.save()
        obj=Food.objects.get(food_id=1)
        order=Order(order_id=1)
        order.save()
        client = Client()
        self.assertEqual(obj.food_id,1)
        response=client.get("/item/counter/1/")
        obj=Food.objects.get(food_id=1)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(obj.counter,1)

    def test_addOrder_works(self):
        objCounter=OrderCounter.objects.create(counter=1)
        objCounter.save()
        client = Client()
        response=client.get("/addFood/")
        self.assertEqual(response.status_code,200)
        obj=Order.objects.create(order_id=1)
        obj.save()
        response=client.get("/addFood/")
        self.assertEqual(response.status_code, 302)
        obj=OrderCounter.objects.get(pk=1)
        self.assertEqual(obj.counter,2)
    
    def test_search(self):
        client = Client()
        response=client.get("/search/",{'keyword':'sate'})
        self.assertEqual(response.status_code,200)
        search=Search.objects.get(keyword='sate')
        self.assertEqual(search.keyword,'sate')
    
    def test_home_exist(self):
        obj=Food(name="makanan",merchant="toko",price=0,calories=0,counter=0,img=get_image_file())
        obj.save()
        client=Client()
        response=client.get("/home/")
        self.assertEqual(response.status_code,200)
    
    @classmethod
    
   
    def get_test_image_file():
        from django.core.files.images import ImageFile
        file = tempfile.NamedTemporaryFile(suffix='.png')
        return ImageFile(file, name=file.name)

    def test_food_pk(self):
        obj=Food.objects.create(name="makanan",merchant="toko",price=0,calories=0,counter=0)
        obj.img=get_image_file()
        obj.save()
        self.assertEqual(obj.pk,1)
    
    def test_food_name(self):
        obj=Food.objects.create(name="makanan",merchant="toko",price=0,calories=0,counter=0)
        obj.img=get_image_file()
        obj.save()
        obj=Food.objects.get(pk=1)
        self.assertEqual(str(obj),"makanan")
    
    def test_if_new_is_in_database(self):
        obj=Food.objects.create(name="makanan",merchant="toko",price=0,calories=0,counter=0)
        count = Food.objects.all().count()
        self.assertEqual(count, 1)
'''
    def test_view_increment(self):
        Client().post()
'''
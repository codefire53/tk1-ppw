# Generated by Django 2.1.5 on 2019-10-15 04:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('pembayaran', '0001_initial'),
        ('sikantin', '0005_food_counter'),
    ]

    operations = [
        migrations.AddField(
            model_name='food',
            name='order',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='pembayaran.Order'),
        ),
    ]

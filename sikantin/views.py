from django.shortcuts import render,redirect
from django.http import HttpResponse,HttpResponseRedirect
from django.views.generic import TemplateView, ListView
from .models import Food, Search
from .forms import SearchForm
from pembayaran.models import Order,OrderCounter
from pemesanan.models import Orderedfood
# Create your views here.
def index(request):
    return render(request,"landing.html")

def increment(request,item_id):
    object = Food.objects.get(food_id=item_id)
    try:
        obj=OrderCounter.objects.get(pk=1)
        curr_id=obj.counter
        order=Order.objects.get(order_id=curr_id)
        ordered_food=Orderedfood.objects.get(curr_id=curr_id,order=order,attr=object)
    except Order.DoesNotExist:
        order=Order(order_id=curr_id)
        order.save()
        ordered_food=Orderedfood(curr_id=curr_id,order=None,attr=object)
        ordered_food.save()
    except Orderedfood.DoesNotExist:
        ordered_food=Orderedfood(curr_id=curr_id,order=None,attr=object)
        ordered_food.save()
    finally:
        ordered_food=Orderedfood.objects.get(curr_id=curr_id,attr=object)
        if(not ordered_food.order==order):
            object.counter=object.counter+1
            object.save()
            ordered_food.order=order
            ordered_food.save()
    return redirect('/home/')

def new_order(request):
    try:
       obj=OrderCounter.objects.get(pk=1)
       curr_id=obj.counter
       order=Order.objects.get(order_id=curr_id)
       obj.counter=obj.counter+1
       obj.save()
       return redirect('/pemesanan/amount')
    except Order.DoesNotExist:
        return HttpResponse("Anda belum memesan apapun :(") 

def homepage(request):
    response = {"search_form": SearchForm}
    response['first_half']=Food.objects.all()[:len(Food.objects.all())//2]
    response['second_half']=Food.objects.all()[len(Food.objects.all())//2:]
    response['recommendations']=Food.objects.all().order_by('-counter')[:3]
    return render(request, 'home.html', response)

def search_results(request):
    response={}
    if request.method=="GET":
        form = SearchForm(request.GET)
        if (form.is_valid()):
            keyword = form.cleaned_data['keyword']
            search=Search(keyword=keyword)
            search.save()
            out=Food.objects.filter(name__icontains=search.keyword)
            response['foods']=out
    return render(request,'search_results.html',response)




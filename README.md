# ppw-tk1-kb07
[![pipeline status](https://gitlab.com/codefire53/tk1-ppw/badges/master/pipeline.svg)](https://gitlab.com/codefire53/tk1-ppw/commits/master)
[![coverage report](https://gitlab.com/codefire53/tk1-ppw/badges/master/coverage.svg)](https://gitlab.com/codefire53/tk1-ppw/commits/master)

Tugas Kelompok 1 PPW 2019

Kelompok: KB07

Link herokuapp: https:/tk1-ppw-kb07.herokuapp.com/

Wireframe:https://wireframe.cc/pro/edit/280040 (User:parspars19, Pass:qwerty0987)

Figma: https://www.figma.com/file/2ARF1fUH05ohNjdON1juER/SiKantin?node-id=0%3A1
## Anggota

1. Mahardika Krisna Ihsani (1806141284)
2. Putri Salsabila (1806186805)
3. Muhammad Ramy Azhar (1806191446)
4. Sean Zeliq Urian (1806141454)

## Nama Aplikasi
SiKantin

## Deskripsi Website

Saat ini, kepadatan Fasilkom UI terutama ketika jam makan siang semakian pada setiap tahunnya, terutama dengan bertambahnya jumlah mahasiswa yang meningkat setiap tahunnya. Keadaan tersebut tentunya dapat berdampak pada beberapa elemen Fasilkom seperti orang mengantri makanan dengan waktu yang lumayan lama. Padahal tidak semua dari mereka mempunyai waktu yang cukup, terutama untuk mahasiswa yang mempunyai waktu isoma yang relatif singkat. Selain itu, pembeli harus melihat-lihat makanan yang tersebut terlebih dahulu sehingga waktu yang terpakai lumayan banyak. Dari permasalahan tersebut, kelompok kami berinisiatif untuk membuat SiKantin. SiKantin merupakan website sistem informasi kantin fasilkom UI. Mengingat tema yang digunakan revolusi industri 4.0, kami mengutilisasi suatu recommender system yang dapat merekomendasikan makanan yang dapat dibeli sehingga pembeli dapat menghemat waktu untuk memilih makanan. 

## Manfaat Website
1. Elemen Fasilkom : Tidak perlu mengantri untuk membeli makanan sehingga dapat mengefisiensi waktu
2. Penjual: Memperoleh laba lebih besar, karena waktu yang dibutuhkan untuk memrproses pemesanan semakin berkurang

## Daftar Fitur
Berikut fitur-fitur yang kami implementasikan:
1. Home (SiKantin), berisi daftar makanan dan rekomendasi makanan
2. Pemesanan, berisi form jumlah makanan yang dipesan
3. Kalori, berisi form data kalori maksimal dari konsumen dan disertai saran kepada konsumen apakah makanan yang dipesan sudah melebihi/kurang dari kalori yang dibutuhkan pemesan
4. Pembayaran, berisi pengisian data yang dibutuhkan untuk proses pembayaran dan pemesanan makanan. Aplikasi ini juga memuat halaman untuk memantau proses pemesanan makanan




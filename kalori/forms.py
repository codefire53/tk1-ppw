from django import forms
class CalorieForm(forms.Form):
    ACTIVITY=(
        ('Tidak Terlalu Aktif','Tidak Terlalu Aktif'),('Lumayan Aktif','Lumayan Aktif'),('Sangat Aktif','Sangat Aktif')
    )
    GENDER=(
        ('Pria','Pria'),('Wanita','Wanita')
    )
    AGE=(
        ('Remaja','Remaja'),('Dewasa','Dewasa'),('Lansia','Lansia')
    )
    age=forms.ChoiceField(choices=AGE,required=False,label="Jenjang Umur")
    gender=forms.ChoiceField(choices=GENDER,required=False,label="Jenis Kelamin")
    activity=forms.ChoiceField(choices=ACTIVITY,required=False,label="Frekuensi Aktivitas")

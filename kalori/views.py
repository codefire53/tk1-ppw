from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.
from pembayaran.models import Order,OrderCounter
from pemesanan.models import Orderedfood
from .forms import CalorieForm
from .models import Calorie
def index(request):
    response={'form':CalorieForm}
    counter=OrderCounter.objects.get(pk=1)
    counterVal=counter.counter-1
    order=Order.objects.get(order_id=counterVal)
    ans=0
    for item in order.orderedfood_set.all():
        ans+=(item.attr.calories*item.amount)
    try:
        calorie=Calorie.objects.get(pk=1)
    except Calorie.DoesNotExist:
        calorie=Calorie(total_calories=0)
        calorie.save()
    finally:
        calorie=Calorie.objects.get(pk=1)
        calorie.total_calories=ans
        calorie.save()
    return render(request,'kalori.html',response)


def get_kalori(request):
    response={'output':'test'}
    output='test'
    if(request.method=='POST'):
        form=CalorieForm(request.POST)
        if(form.is_valid()):
            age=form.cleaned_data['age']
            activity=form.cleaned_data['activity']
            gender=form.cleaned_data['gender']
            calorie=Calorie.objects.get(pk=1)
            calorie_value=calorie.total_calories
            if(gender=='Pria'):
                if(age=='Remaja'):
                    if(activity=='Tidak Terlalu Aktif'):
                        low_limit=2400
                        high_limit=2400
                    elif(activity=='Lumayan Aktif'):
                        low_limit=2600
                        high_limit=2800
                    elif(activity=='Sangat Aktif'):
                        low_limit=3000
                        high_limit=3000
                elif(age=='Dewasa'):
                    if(activity=='Tidak Terlalu Aktif'):
                        low_limit=2200
                        high_limit=2200
                    elif(activity=='Lumayan Aktif'):
                        low_limit=2400
                        high_limit=2600
                    elif(activity=='Sangat Aktif'):
                        low_limit=2800
                        high_limit=3000
                else:                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
                    if(activity=='Tidak Terlalu Aktif'):
                        low_limit=2000
                        high_limit=2000
                    elif(activity=='Lumayan Aktif'):
                        low_limit=2200
                        high_limit=2400
                    elif(activity=='Sangat Aktif'):
                        low_limit=2400
                        high_limit=2800
            
            else:
                if(age=='Remaja'):
                    if(activity=='Tidak Terlalu Aktif'):
                        low_limit=2000
                        high_limit=2000
                    elif(activity=='Lumayan Aktif'):
                        low_limit=2000
                        high_limit=2200
                    elif(activity=='Sangat Aktif'):
                        low_limit=2400
                        high_limit=2400
                elif(age=='Dewasa'):
                    if(activity=='Tidak Terlalu Aktif'):
                        low_limit=1800
                        high_limit=1800
                    elif(activity=='Lumayan Aktif'):
                        low_limit=2000
                        high_limit=2000
                    elif(activity=='Sangat Aktif'):
                        low_limit=2200
                        high_limit=2200
                else:                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
                    if(activity=='Tidak Terlalu Aktif'):
                        low_limit=1600
                        high_limit=1600
                    elif(activity=='Lumayan Aktif'):
                        low_limit=1800
                        high_limit=1800
                    elif(activity=='Sangat Aktif'):
                        low_limit=2000
                        high_limit=2200
            if(calorie_value >= low_limit and calorie_value <=high_limit):
                output="Kalori Anda sudah terpenuhi"
            elif(calorie_value > high_limit):
                output="Makanan yang Anda pesan sudah melebihi {} dari batas kalori".format(calorie_value-high_limit)
            else:
                output="Makanan yang Anda pesan masih kurang {} dari batas kalori".format(low_limit-calorie_value)
    return HttpResponse(output) 


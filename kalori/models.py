from django.db import models

# Create your models here.
class Calorie(models.Model):
    total_calories=models.PositiveIntegerField(default=1,blank=True,null=True)
    class Meta:
        verbose_name_plural = "total_calories"
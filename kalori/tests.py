from django.test import TestCase,Client
from pembayaran.models import Order,OrderCounter
from pemesanan.models import Orderedfood
from sikantin.models import Food
from .forms import CalorieForm
from .models import Calorie
# Create your tests here.
class KaloriModelTest(TestCase):
    def test_index_calorie_exist_functionality(self):
        calorie=Calorie(total_calories=0)
        calorie.save()
        counter=OrderCounter(counter=2)
        counter.save()
        order=Order(order_id=1)
        order.save()
        food=Food(food_id=1,name='sate',merchant='store',price=0,calories=2.00,counter=1)
        food.save()
        of=Orderedfood(curr_id=1,amount=2,attr=food,order=order)
        of.save()
        order=Order.objects.get(order_id=1)
        response = Client().get('/kalori/')
        self.assertEqual(response.status_code, 200)
        calorie=Calorie.objects.get(pk=1)
        self.assertEqual(calorie.total_calories,4.00)
        
    def test_index_calorie_notexist_functionality(self):
        counter=OrderCounter(counter=2)
        counter.save()
        order=Order(order_id=1)
        order.save()
        food=Food(food_id=1,name='sate',merchant='store',price=0,calories=2.00,counter=1)
        food.save()
        of=Orderedfood(curr_id=1,amount=2,attr=food,order=order)
        of.save()
        order=Order.objects.get(order_id=1)
        response = Client().get('/kalori/')
        self.assertEqual(response.status_code, 200)
        calorie=Calorie.objects.get(pk=1)
        self.assertEqual(calorie.total_calories,4.00)
    
    def test_calorie_response(self):
        c=Client()
        calorie=Calorie(total_calories=2200)
        calorie.save()
        response=c.post('/kalori/getkalori/',{'age':'Dewasa','activity':'Tidak Terlalu Aktif','gender':'Pria'})
        self.assertEqual(response.status_code,200)
        self.assertEqual(response.content,b'Kalori Anda sudah terpenuhi')
        response=c.post('/kalori/getkalori/',{'age':'Lansia','activity':'Tidak Terlalu Aktif','gender':'Pria'})
        self.assertEqual(response.status_code,200)
        self.assertEqual(response.content,b"Makanan yang Anda pesan sudah melebihi 200 dari batas kalori")
        response=c.post('/kalori/getkalori/',{'age':'Remaja','activity':'Sangat Aktif','gender':'Pria'})
        self.assertEqual(response.status_code,200)
        self.assertEqual(response.content,b"Makanan yang Anda pesan masih kurang 800 dari batas kalori")
        
